<?php

namespace Database\Seeders;

use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Support\Facades\Log;
use Sentinel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use mysql_xdevapi\Exception;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        try{

            DB::beginTransaction();
            $adminpermission = [];
            foreach (config('role-permission') as $key => $value) {
                foreach ($value['data'] as $ky => $val) {
                    $adminpermission[$val['permission']] = true;
                }
            }

            $adminRole = Sentinel::getRoleRepository()->createModel()
                ->create(array(
                    'name' => 'Admin',
                    'slug' => 'admin',
                    'permissions' => $adminpermission
                ));

            DB::table('users')->insert([
                'id'=>1,
                'email' => 'admin@gmail.com',
                'password' => Hash::make('password'),
                'first_name'=>'admin',
                'last_name'=>'super',
                'image'=>'images/1.jpg',
                'gender'=>'male',
                'hobbies'=>'reading,coding',
            ]);
            $user = Sentinel::findUserById(1);
            $activation = Activation::create($user);
            Activation::complete($user,$activation->code);
            $role = Sentinel::findRoleBySlug('admin');
            $role->users()->attach($user);
            DB::commit();

        }
        catch(\Exception $e)
        {
            \Log::info($e->getMessage());
            DB::rollBack();
        }
    }
}