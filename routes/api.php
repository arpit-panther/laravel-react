<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//
//Route::middleware('LoginMiddleware',function (){
//    Route::resource('/user',UserController::class);
//
//    Route::post('/login',[UserController::class,'login']);
//});



Route::group(['middleware'=>['auth:sanctum,role:admin,user,manager']],function (){
    Route::resource('/user',UserController::class);
    Route::resource('/role',RoleController::class);
    Route::resource('/post',PostController::class);
    Route::get('/rolesname',[RoleController::class,'roleName'])->name('rolesname');
    Route::get('/userrole/{id}',[UserController::class,'userrole'])->name('userrole');
    Route::put('/updaterole/{id}',[UserController::class,'updaterole'])->name('updaterole');
});

Route::post('/login',[UserController::class,'login']);

Route::post('/register',[UserController::class,'register'])->name('user.register');

Route::get('count',[UserController::class,'count']);

Route::get('test',[TestController::class,'test']);