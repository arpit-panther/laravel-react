<?php

$module = "Module";
return [

    /**
     * For Home Module
     *
     */
    [
        'label' => 'Manage Post',
        'data'  => [
            [
                'label'      => $module,
                'permission' => 'view_post_module',
            ],
            [
                'label'      => 'View',
                'permission' => 'view_post',
            ],
            [
                'label'      => 'Create',
                'permission' => 'create_post',
            ],
            [
                'label'      => 'Update',
                'permission' => 'update_post',
            ],
            [
                'label'      => 'Delete',
                'permission' => 'delete_post',
            ]
        ],
    ],

    [
        'label' => 'Manage Users',
        'data'  => [
            [
                'label'      => $module,
                'permission' => 'view_user_module',
            ],
            [
                'label'      => 'View',
                'permission' => 'view_user',
            ],
            [
                'label'      => 'Create',
                'permission' => 'create_user',
            ],
            [
                'label'      => 'Update',
                'permission' => 'update_user',
            ],
            [
                'label'      => 'Delete',
                'permission' => 'delete_user',
            ]
        ],
    ],


    [
        'label' => 'Manage Roles',
        'data'  => [
            [
                'label'      => $module,
                'permission' => 'view_role_module',
            ],
            [
                'label'      => 'View',
                'permission' => 'view_role',
            ],
            [
                'label'      => 'Create',
                'permission' => 'create_role',
            ],
            [
                'label'      => 'Update',
                'permission' => 'update_role',
            ],
            [
                'label'      => 'Delete',
                'permission' => 'delete_role',
            ]
        ],
    ],
];
