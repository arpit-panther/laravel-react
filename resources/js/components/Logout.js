import React from 'react';
import {Redirect,useHistory} from 'react-router-dom';


const Logout = () => {
    // debugger;
    let history = useHistory();
    sessionStorage.clear();
    console.log('session storage--',sessionStorage);
    console.log('history before logout',history);
    // window.location.reload();
    history.push('/login');
    return (null);
};
export default Logout;
