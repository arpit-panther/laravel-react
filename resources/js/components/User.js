import React,{useState,useEffect} from 'react';
import axios from 'axios';
import {Link,useHistory} from 'react-router-dom';
import {Button} from "react-bootstrap";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content';
import Pagination from 'react-js-pagination';

window.React = React;

const User = () => {
    let history = useHistory();

    const MySwal = withReactContent(Swal);
    const Hello  = React.useCallback(()=>{
        loadUsers();
    });
    const [users,setUser] = useState([]);
    console.log('Users',users);


    const [total,setTotal] = useState();

    const perpage = 5 ;
    const [currentPage, setCurrentPage] = useState(1);

    const [page,setPage] = useState(1);

    function deleteUser (id) {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                const res =  axios.delete(`api/user/${id}`,config);
                loadUsers();
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        });
    }

    function handleSearch(page){
        setPage(page);
        setCurrentPage(page);
    }

    useEffect(() => {
        loadUsers();
        Hello();
    },[page]);

    const loadUsers = async () => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        let  params = {
                perpage: perpage,
                    page : page,
        };

        let url = 'api/user?'+ new URLSearchParams(params).toString();
        const res = await axios.get(url,config)
            .then( function (res) {
                const result =  res.data.data.data;
                setUser(result);
                console.log('result of users-->',result);

                const count = axios.get('api/count');
                setTotal(count.data);
            })
            .catch(function (error) {
                console.log(error);
                if (error.response.status === 401) {
                    let timerInterval
                    Swal.fire({
                        title: 'Oops!! You Have No Access To THis Page',
                        timer: 1000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                    });
                    history.push('/');
                }
            });


    };

    return(
        <div className="container">
            <div className="py-4">
                <h1>Home page</h1>
                <table className="table border shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Email</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Hobbies</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        users && users.map((user,index) => (
                            <tr key={index}>
                                <th scope="row">{index+1}</th>
                                <th>{user.email}</th>
                                <th>{user.first_name}</th>
                                <th>{user.last_name}</th>
                                <th>{user.gender}</th>
                                <th>{user.hobbies}</th>
                                <td>
                                    <Link to={`/users/${user.id}`}>
                                        <Button className="btn btn-primary mr-2"> View </Button>
                                    </Link>
                                    <Link to={`/users/edit/${user.id}`}>
                                        <Button className="btn btn-warning mr-2"> Edit </Button>
                                    </Link>
                                    <Button className="btn btn-danger" onClick={() => deleteUser(user.id)}> Delete </Button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                <Pagination
                    activePage={currentPage}
                    totalItemsCount={total}
                    itemsCountPerPage={perpage}
                    onChange={(e) => handleSearch(e)}
                    itemClass="page-item"
                    linkClass="page-link"
                    firstPageText="First"
                    lastPageText="Last"
                />
            </div>
        </div>
    );
};

export default User;