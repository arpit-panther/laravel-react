import React,{useState,useEffect} from 'react';
import axios from 'axios';
import {Link,useHistory} from 'react-router-dom';
import {Button} from "react-bootstrap";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content';

window.React = React;

const Roles = () => {
    let history = useHistory();

    const MySwal = withReactContent(Swal);

    const [roles,setRoles] = useState([]);


    function deleteRole (id) {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                const res =  axios.delete(`api/role/${id}`,config);
                loadRoles();
                Swal.fire(
                    'Deleted!',
                    'Your Role has been deleted.',
                    'success'
                )
            }
        });
    }


    useEffect(() => {
        loadRoles();
    },[]);




    const loadRoles =  async () => {
        const token = sessionStorage.getItem('token');
        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        let url = 'api/role?' + new URLSearchParams().toString();
        const res = await axios.get(url, config)
            .then( function (res) {
                const result = res.data;
                setRoles(result);
            })
            .catch(function (error) {
                if (error.response.status === 401) {
                let timerInterval
                Swal.fire({
                    title: 'Oops!! You Have No Access To THis Page',
                    timer: 1000,
                    timerProgressBar: true,
                    didOpen: () => {
                        Swal.showLoading()
                        timerInterval = setInterval(() => {
                            const content = Swal.getContent()
                            if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                    b.textContent = Swal.getTimerLeft()
                                }
                            }
                        }, 100)
                    },
                    willClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then((result) => {
                    /* Read more about handling dismissals below */
                    if (result.dismiss === Swal.DismissReason.timer) {
                        console.log('I was closed by the timer')
                    }
                });
                    history.push('/');
                }
            });
    };


    return(
        <div className="container">
            <div className="py-4">
                <h1>Manage Roles</h1>
                <table className="table border shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        roles && roles.map((role,index) => (
                            <tr key={index}>
                                <th scope="row">{index+1}</th>
                                <th>{role.name}</th>
                                <td>
                                    <Link to={`/roles/${role.id}`}>
                                        <Button className="btn btn-primary mr-2"> View </Button>
                                    </Link>
                                    <Link to={`/role/edit/${role.id}`}>
                                        <Button className="btn btn-warning mr-2"> Edit </Button>
                                    </Link>
                                    <Button className="btn btn-danger" onClick={() => deleteRole(role.id)}> Delete </Button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                <Link to={'addRoles'}>
                    <Button className="nav-link" >Add Role</Button>
                </Link>
            </div>
        </div>
    );
};

export default Roles;