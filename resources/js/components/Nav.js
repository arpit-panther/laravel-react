import React from "react";
import {Link} from "react-router-dom";


const Nav = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">

            {
                sessionStorage.getItem('token') !== null ?
                    <div className="container">
                        <Link className="navbar-brand" to="/">Home</Link>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarColor01">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item active">
                                    <Link className="nav-link" to="/addUsers">Add User
                                        <span className="sr-only">(current)</span>
                                    </Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/posts" >Posts</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/Roles" >Roles</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/Logout" >Logout</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    :
                    <div className="container">
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarColor01">
                            <ul className="navbar-nav mr-auto">
                                {/*<li className="nav-item">*/}
                                    {/*<Link className="nav-link" to="/register" >Register</Link>*/}
                                {/*</li>*/}
                                <li className="nav-item">
                                    <Link className="nav-link" to="/login" >Login</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
            }
        </nav>
    )
};
export default Nav;
