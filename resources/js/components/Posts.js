import React,{useState,useEffect} from 'react';
import axios from 'axios';
import {Link,useHistory} from 'react-router-dom';
import {Button} from "react-bootstrap";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content';

window.React = React;

const Posts = () => {
    let history = useHistory();

    const MySwal = withReactContent(Swal);

    const [posts,setPosts] = useState([]);


    function deletePost (id) {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                const res =  axios.delete(`api/post/${id}`,config)
                    .then(function (res) {
                        loadPosts();
                        Swal.fire(
                            'Deleted!',
                            'Your Role has been deleted.',
                            'success'
                        )
                    })
                    .catch(function (error) {
                        if (error.response.status === 401) {
                            let timerInterval
                            Swal.fire({
                                title: 'Oops!! You Have No Access To Delete Post',
                                timer: 1000,
                                timerProgressBar: true,
                                didOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                        const content = Swal.getContent()
                                        if (content) {
                                            const b = content.querySelector('b')
                                            if (b) {
                                                b.textContent = Swal.getTimerLeft()
                                            }
                                        }
                                    }, 100)
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then((result) => {
                                /* Read more about handling dismissals below */
                                if (result.dismiss === Swal.DismissReason.timer) {
                                    console.log('I was closed by the timer')
                                }
                            });
                            loadPosts();
                        }
                    });
            }
        });
    }


    useEffect(() => {
        loadPosts();
    },[]);




    const loadPosts =  async () => {
        const token = sessionStorage.getItem('token');
        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        let url = 'api/post?' + new URLSearchParams().toString();
        const res = await axios.get(url, config)
            .then( function(res){
                const result = res.data.data;
                let array = Object.values(result);
                setPosts(array);
            })
            .catch(function (error) {
                if (error.response.status === 401) {
                    let timerInterval
                    Swal.fire({
                        title: 'Oops!! You Have No Access To THis Page',
                        timer: 1000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                    });
                    history.push('/');
                }
            });
    };

    return(
        <div className="container">
            <div className="py-4">
                <h1>Manage Posts</h1>
                <table className="table border shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tite</th>
                        <th scope="col">Image</th>
                        <th scope="col">Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        posts && posts.map((post,index) => (
                            <tr key={index}>
                                <th scope="row">{index+1}</th>
                                <th>{post.title}</th>
                                <img src={post.image} height="100px" width="100px" />
                                <th>{post.description}</th>
                                <td>
                                    <Link to={`/posts/${post.id}`}>
                                        <Button className="btn btn-primary mr-2"> View </Button>
                                    </Link>
                                    <Link to={`/post/edit/${post.id}`}>
                                        <Button className="btn btn-warning mr-2"> Edit </Button>
                                    </Link>
                                    <Button className="btn btn-danger" onClick={() => deletePost(post.id)}> Delete </Button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                <Link to={'addPosts'}>
                    <Button className="nav-link" >Add Posts</Button>
                </Link>
            </div>
        </div>
    );
};

export default Posts;