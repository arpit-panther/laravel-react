import React,{useRef,useState,useEffect} from 'react';
import {Link, useHistory} from 'react-router-dom';
import { Checkbox } from '@material-ui/core';
import {useForm} from "react-hook-form";
import classNames from 'classnames';
import axios from "axios";
import Swal from "sweetalert2";




const AddUser = () => {
    let history = useHistory();
    const [hobbies, setHobbies] = useState([]);
    const [errors,setError] = useState(null);
    const [roles,setRoles] = useState();
    const [role,setRole] = useState([]);

    const [user,setUser] = useState({
        email:"",
        password:"",
        password_confirmation:"",
        first_name:"",
        last_name:"",
        image:"",
        gender:"",
        hobbies:"",
    });

    const [tosendrol,setToSendRole] = useState({
       roles:"",
    });




    useEffect(() => {
        const token = sessionStorage.getItem('token');
        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        let data = "test";
        axios.post("api/user",data,config)
            .then( function () {

            })
            .catch(function (error) {
                if (error.response.status === 401) {
                    let timerInterval
                    Swal.fire({
                        title: 'Oops!! You Have No Access To THis Page',
                        timer: 1000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                    });
                    history.push('/');
                }
            });
        getRoles();
        return () => setUser({...user,hobbies:hobbies});
    }, [hobbies]);

    const getValue = (e) => {
        let valuehob = e.target.value;
        if(hobbies.includes(valuehob)){
            let store =  hobbies.filter(hobby => hobby !== valuehob);
            hobbies = store;
        }
        else {
            hobbies.push(valuehob);
        }
        setUser({...user,hobbies:hobbies});
    };




    useEffect(() => {
        setToSendRole({...tosendrol,roles:role});
        return () => setToSendRole({...tosendrol,roles:role});
    }, [role]);

    const saveRole =(e) => {
        let value = e.target.value;
        if(role.includes(value)){
            let store =  role.filter(hobby => hobby !== value);
            role = store;
        }
        else {
            role.push(value);
        }
        setToSendRole({...tosendrol,roles:role});
    };




    const getRoles = async () => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };

        let url = 'api/rolesname?' + new URLSearchParams().toString();
        const res = await axios.get(url, config);
        const result = res.data.data;
        setRoles(result);
    };

    const fileUpload = (e) => {
            const file = e.target.files[0];
            setUser({...user,image:file});
    };



    const changeInput = event => {
        setUser({...user,[event.target.name]:event.target.value});
        console.log(user);
    };


    const onsubmit = async (event) => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };


        const data = new FormData();
        console.log('data at init',data);
        data.append('email', user.email);
        data.append('password', user.password);
        data.append('password_confirmation', user.password_confirmation);
        data.append('first_name', user.first_name);
        data.append('last_name', user.last_name);
        data.append('image', user.image);
        data.append('gender', user.gender);
        data.append('roles', tosendrol.roles);
        data.append('hobbies', user.hobbies);

        console.log('data-----',data);
            await axios.post("api/user",data,config)
            .then(function (res){
                history.push("/");
            })
            .catch(function (error) {
                if (error.response) {
                    const new_error = error.response.data.errors;
                        setError(new_error)
                }
            })
    };

    const {register,handleSubmit,watch,clearError} = useForm({
        mode:"onTouched",
    });

    const password = useRef({});
    password.current = watch("password", "");

    return(
        <div className="container mt-5">
            <form name="form" className="form" onSubmit={handleSubmit( event => onsubmit(event))}>
            <fieldset>


                <div className="form-group">
                    <label>Email address</label>
                    <input type="email"
                           className={classNames("form-control")}
                           id="email" name="email"
                           onChange={event => changeInput(event)}
                           placeholder="Enter email"/>

                    {/*{*/}
                        {/*errors && errors.email ?*/}
                            {/*<div className="invalid-feedback" style={{display:'block'}}>*/}
                                {/*{errors.email}*/}
                            {/*</div>*/}
                            {/*: null*/}
                    {/*}*/}

                </div>


                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input type="password"
                           className={classNames("form-control")}
                           id="password" name="password"
                           onChange={event => changeInput(event)}
                           placeholder="Password"/>

                    {
                        errors && errors.password ?
                            <div className="invalid-feedback" style={{display:'block'}}>
                                {errors.password}
                            </div>
                            : null
                    }
                </div>


                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Password Confirmation</label>
                    <input type="password"
                           className={classNames("form-control")}
                           id="password_confirmation" name="password_confirmation"
                           onChange={event => changeInput(event)}
                           placeholder="Confirm Password"/>

                    {/*{*/}
                    {/*errors && errors.cpassword ?*/}
                    {/*<div className="invalid-feedback" style={{display:'block'}}>*/}
                    {/*{errors.cpassword}*/}
                    {/*</div>*/}
                    {/*: null*/}
                    {/*}*/}
                </div>


                <div className="form-group">
                    <label>First Name</label>
                    <input type="text"
                           className={classNames("form-control")}
                           id="first_name"
                           name="first_name"
                           onChange={event => changeInput(event)}
                           placeholder="Enter First Name"/>

                    {
                        errors && errors.first_name ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.first_name}
                                </div>
                                : null
                    }
                </div>


                <div className="form-group">
                    <label>Last Name</label>
                    <input type="text"
                           className={classNames("form-control")}
                           id="last_name"
                           name="last_name"
                           onChange={event => changeInput(event)}
                           placeholder="Enter Last Name"/>

                    {
                        errors && errors.last_name ?
                            <div className="invalid-feedback" style={{display:'block'}}>
                                {errors.last_name}
                            </div>
                            : null
                    }
                </div>


                <div className="form-group">
                    <label>Image</label>
                    <input type="file"
                           className={classNames("form-control")}
                           id="image"
                           name="image"
                           onChange={fileUpload}/>

                    {
                        errors && errors.image ?
                            <div className="invalid-feedback" style={{display:'block'}}>
                                {errors.image}
                            </div>
                            : null
                    }

                </div>


                <div className="form-group">
                    <label> Please select a Gender </label><br/>
                    <input type="radio"
                           value="male"
                           id="male"
                           name="gender"
                           onChange={event => changeInput(event)} />Male
                    <input type="radio" value="female"
                           id="female" name="gender"
                           onChange={event => changeInput(event)} />Female
                    {
                        errors && errors.gender ?
                            <div className="invalid-feedback" style={{display:'block'}}>
                                {errors.gender}
                            </div>
                            : null
                    }
                </div>


                {/*<div className="form-group">*/}
                    {/*<select multiple="" className="custom-select" name="roles" onChange={event => changeInput(event)}>*/}
                        {/*<option value="">Select A Role</option>*/}
                        {/*{*/}
                            {/*roles && roles.map((role,index) => (*/}
                                {/*<option value={role.id} key={role.id}>{role.name}</option>*/}
                            {/*))*/}
                        {/*}*/}
                    {/*</select>*/}
                {/*</div>*/}

                <div className="form-group">
                    <label> Select Your Roles </label><br/>
                    {
                        roles && roles.map((role) => (
                            <div>
                                <input type="checkbox" id="roles" name="roles"
                                       value={role.id} key={role.id} onChange={(e) => saveRole(e)}
                                />
                                {role.name}
                            </div>
                        ))
                    }
                </div>

                <div className="form-group">
                    <label> Please select a Hobbies </label><br/>
                    <Checkbox
                        color="primary"
                        value="reading"
                        onChange={(e) => getValue(e)}
                    />Reading
                    <Checkbox
                        color="primary"
                        value="coding"
                        onChange={(e) => getValue(e)}
                    />Coding
                    <Checkbox
                        color="primary"
                        value="dancing"
                        onChange={(e) => getValue(e)}
                    />dancing
                    <Checkbox
                        color="primary"
                        value="cooking"
                        onChange={(e) => getValue(e)}
                    />Cooking
                    {
                        errors && errors.hobbies ?
                            <div className="invalid-feedback" style={{display:'block'}}>
                                {errors.hobbies}
                            </div>
                            : null
                    }
                </div>


                <Link className="btn btn-warning" to="/">
                    Back To Home
                </Link>

                <button type="submit" className="btn btn-primary">Submit</button>
            </fieldset>
        </form>
        </div>
    );
};

export default AddUser;
