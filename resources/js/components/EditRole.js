import React,{useRef,useState,useEffect} from 'react';
import {Link, useHistory,useParams} from 'react-router-dom';
import { Checkbox } from '@material-ui/core';
import {useForm} from "react-hook-form";
import classNames from 'classnames';
import axios from "axios";
import Table from 'react-bootstrap/Table';
import Swal from "sweetalert2";



const EditRole = (match) => {
    let params = match.params;
    let history = useHistory();
    let {id} = useParams();

    const [rolepermission,setRolePermission] = useState();
    const [permissions,setPemissions] = useState([]);
    // const [perm,setPerm] = useState([]);

    const [roles,setRoles] = useState({
        name:"",
        permissions:"",
    });

    useEffect( () => {
        const token = sessionStorage.getItem('token');
        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        let data = "test";
        const result = axios.get(`api/role/${id}/edit`,config)
        .then( function () {

        })
        .catch(function (error) {
            if (error.response.status === 401) {
                let timerInterval
                Swal.fire({
                    title: 'Oops!! You Have No Access To THis Page',
                    timer: 1000,
                    timerProgressBar: true,
                    didOpen: () => {
                        Swal.showLoading()
                        timerInterval = setInterval(() => {
                            const content = Swal.getContent()
                            if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                    b.textContent = Swal.getTimerLeft()
                                }
                            }
                        }, 100)
                    },
                    willClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then((result) => {
                    /* Read more about handling dismissals below */
                    if (result.dismiss === Swal.DismissReason.timer) {
                    }
                });
                history.push('/');
            }
        });
        loadRoles();
        getPermission();
        // setRoles({...roles,permissions:perm});
        // return () => setRoles({...roles,permissions:perm})
    },[]);



    const loadRoles = async () => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        const result = await axios.get(`api/role/${id}/edit`,config);
        const data = result.data.data;
        const roleName = data.role.name;
        const rolepermission = Object.keys(data.rolepermission);
        setRoles({name:roleName,permissions:rolepermission});
        setRolePermission(rolepermission);
    };



    const getPermission = async (event) => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        let url = 'api/role/create?' + new URLSearchParams().toString();
        const res = await axios.get(url, config);
        setPemissions(res.data.data);
    };




    let permi = [];
    const getValue = (e, index,index2) => {


        if(index2 === 0){
            const roPer = roles.permissions;
            const perInData = permissions[index].data[0].permission;

            if(roPer.includes(perInData)){

                let specific = [];
                specific = roles.permissions;

                let storee = [];
                permissions[index].data && permissions[index].data.map((newPer,ind) => {
                        (roles.permissions.includes(newPer.permission))
                        ?
                            specific = (storee = specific.filter(perm => perm !== newPer.permission))
                        :
                            null

                });
                setRoles({...roles,permissions: specific});
            }
            else {
                let value = e.target.value;
                permi = roles.permissions;
                if(permi.includes(value)){
                    let store =  permi.filter(per => per !== value);
                    permi = store;
                }
                else {
                    permi.push(value);
                }
                setRoles({...roles,permissions: permi});
            }
        }
        else {
            if (index2 === 1 || 2 || 3 || 4 ){
                const roPer = roles.permissions;
                const perInData = permissions[index].data[0].permission;
                let value = e.target.value;
                permi = roles.permissions;
                if (permi.includes(perInData)){
                }
                else {
                    permi.push(perInData);
                }
                if(permi.includes(value)){
                    let store =  permi.filter(per => per !== value);
                    permi = store;
                }
                else {
                    permi.push(value);
                }
                setRoles({...roles,permissions: permi});
            }

        }
    };





    const changeInput = event => {
        setRoles({...roles,[event.target.name]:event.target.value});
    };



    const onsubmit = async (event) => {
        event.preventDefault();
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        const result = await axios.put(`api/role/${id}`,roles,config);
        if(result.status === 200){
            history.push("/Roles");
        }
    };


    const changeClass = () => {
        //     fsdfsd
        // return some + "checkeditemp
    };


    const {handleSubmit} = useForm({
        mode:"onTouched",
    });


    return(
        <div className="container mt-5">
            {/*<form name="form" className="form" onSubmit={handleSubmit( event => onsubmit(event))}>*/}
            <form onSubmit={event => onsubmit(event)}>
            <fieldset>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text"
                               className={classNames("form-control")}
                               id="name"
                               name="name"
                               value={roles.name}
                               onChange={event => changeInput(event)}
                               placeholder="Enter Name"/>

                    </div>

                    <Table striped bordered hover variant="dark">
                        <thead>
                        <tr>
                            <th>Module</th>
                            <th>Permission</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            permissions && permissions.map( (permission,index) => (
                                <tr key={index}>
                                    <td>{permission.label}</td>
                                    {
                                        permission.data && permission.data.map((permissionDetail,index2) => (
                                            <td key={index2}>
                                                <input type="checkbox"
                                                       id="permission"
                                                       name="permission"
                                                       className={ index2 === 0 ? changeClass():null }
                                                       value={permissionDetail.permission}
                                                       checked={roles.permissions.includes(permissionDetail.permission)}
                                                       onChange={(e) => getValue(e, index,index2)}
                                                />
                                                <span className="label-text">{permissionDetail.label}</span>
                                            </td>
                                        ))
                                    }
                                </tr>
                            ))
                        }
                        </tbody>
                    </Table>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </fieldset>
            </form>
        </div>
    );
};

export default EditRole;
