import React,{useRef,useState,useEffect} from 'react';
import {Link, useHistory} from 'react-router-dom';
import { Checkbox } from '@material-ui/core';
import {useForm} from "react-hook-form";
import classNames from 'classnames';
import axios from "axios";
import Table from 'react-bootstrap/Table';
import Swal from "sweetalert2";



const AddPosts = () => {
    let history = useHistory();

    const [posts,setPosts] = useState({
        title:"",
        image:"",
        description:"",
    });

    useEffect( () => {
        const token = sessionStorage.getItem('token');
        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        let data = "test";
        axios.post("api/post",data,config)
            .then( function () {

            })
            .catch(function (error) {
                if (error.response.status === 401) {
                    let timerInterval
                    Swal.fire({
                        title: 'Oops!! You Have No Access To THis Page',
                        timer: 1000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                    });
                    history.push('/');
                }
            });
    });

    const fileUpload = (e) => {
        const file = e.target.files[0];
        setPosts({...posts,image:file});
    };



    const changeInput = event => {
        setPosts({...posts,[event.target.name]:event.target.value});
        console.log('posts->',posts);
    };


    const onsubmit = async (event) => {
        event.preventDefault();
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        const data = new FormData();
        data.append('title', posts.title);
        data.append('image', posts.image);
        data.append('description', posts.description);
        const result = await axios.post("api/post",data,config);
        if(result.status === 200){
            history.push("/Posts");
        }
        console.log('roles-result->',result);
    };

    const {handleSubmit} = useForm({
        mode:"onTouched",
    });

    return(
        <div className="container mt-5">
            <form onSubmit={event => onsubmit(event)}>
                <fieldset>

                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Image Title</label>
                        <input type="text"
                               className="form-control"
                               id="title" name="title"
                               onChange={event => changeInput(event)} placeholder="Enter Image title"/>
                        {/*{*/}
                            {/*errors && errors.title ?*/}
                                {/*<div className="invalid-feedback" style={{display:'block'}}>*/}
                                    {/*{errors.title}*/}
                                {/*</div>*/}
                                {/*: null*/}
                        {/*}*/}
                    </div>

                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Image</label>
                        <input type="file"
                               className="form-control"
                               id="image" name="image"  onChange={fileUpload} placeholder="Enter image"/>
                        {/*{*/}
                            {/*errors && errors.file ?*/}
                                {/*<div className="invalid-feedback" style={{display:'block'}}>*/}
                                    {/*{errors.file}*/}
                                {/*</div>*/}
                                {/*: null*/}
                        {/*}*/}
                    </div>


                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Description</label>
                        <input type="text"
                               className="form-control"
                               id="description" name="description"
                               onChange={event => changeInput(event)} placeholder="Enter Description"/>
                        {/*{*/}
                            {/*errors && errors.description ?*/}
                                {/*<div className="invalid-feedback" style={{display:'block'}}>*/}
                                    {/*{errors.description}*/}
                                {/*</div>*/}
                                {/*: null*/}
                        {/*}*/}
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </fieldset>
            </form>
        </div>
    );
};

export default AddPosts;
