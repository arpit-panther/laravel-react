import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router,Switch,Route} from "react-router-dom";
import Nav from "./Nav";
import User from "./User";
import AddUser from "./AddUser";


function Example() {
    return (
        <Router>
            <>
                <Nav/>
                <Switch>
                    <Route path="/" exact component={User} />
                    <Route path="/addUsers" exact component={AddUser} />
                </Switch>
            </>
        </Router>
    );
}



export default Example;

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}
