import React, {useEffect, useState} from 'react';
import axios from "axios";
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2'
import withReactContent from "sweetalert2-react-content";


const Login = () => {

    const MySwal = withReactContent(Swal);

    function errorCall() {
        // Swal.fire('Your Login Credentials Does Not Match Our Record..')
        Swal.fire({
            title: 'Your Login Credentials Does Not Match Our Record..',
            text: 'Please check your email and password',
            width: 600,
            padding: '3em',
            background: '#fff url(/images/trees.png)',
            backdrop:
                `rgba(94,182,157,0.2)
                url("https://media.giphy.com/media/xFnyJDPKOcqcH57m7e/giphy.gif")
                left top
                no-repeat`
        })
    }

    let history = useHistory();

        const [user,setUser] = useState({
        email:"",
        password:"",
    });


    const onChangeInput = event => {
        setUser({...user,[event.target.name]:event.target.value});
    };

    const onsubmit = async (event) => {
        event.preventDefault();

        axios.get('/sanctum/csrf-cookie')
            .then(response => {
                axios.post("/api/login",user)
                    .then(response => {
                        if(response.status === 200 ){
                            sessionStorage.setItem("token", response.data.token);
                            // window.location.reload();
                            history.push("");
                        }
                        else{
                            // setError('error');
                            errorCall();
                        }
                    });
        });
    };

    return(
        <div className="container mt-5">
            <form name="form" className="form" onSubmit={(event => onsubmit(event))}>
                <fieldset>
                    <div>
                        <h1>Login Here</h1>
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Email address</label>
                        <input type="email"
                               className="form-control"
                               id="email"
                               name="email"
                               onChange={event => onChangeInput(event)}
                               placeholder="Enter email"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input type="password"
                               className="form-control"
                               id="password"
                               name="password"
                               onChange={event => onChangeInput(event)}
                               placeholder="Password"/>
                    </div>
                    <button type="submit" className="btn btn-primary">Login</button>
                </fieldset>
            </form>
        </div>
    );
};

export default Login;
