
import React,{useRef,useState,useEffect} from 'react';
import {Link, useHistory} from 'react-router-dom';
import { Checkbox } from '@material-ui/core';
import {useForm} from "react-hook-form";
import classNames from 'classnames';                       <tbody>
                        {
                            permissions && permissions.map( (permission) => (
                                <tr>
                                    <td>{permission.label}</td>
                                    {
                                        permission.data && permission.data.map((permissionDetail) => (
                                            <td>
                                                <input type="checkbox" id="permission" name="permission" value={permissionDetail.permission} onChange={(e) => getValue(e)} />
                                                <span className="label-text">{permissionDetail.label}</span>
                                            </td>
                                        ))
                                    }
                                </tr>
                            ))
                        }
                        </tbody>
import axios from "axios";



const AddUser = () => {
    let history = useHistory();
    const [hobbies, setHobbies] = useState([]);
    const [error,setError] = useState("");
    const [image,setImage] = useState("");

    const [user,setUser] = useState({
        name:"",
        email:"",
        image:"",
        gender:"",
        hobbies:"",
        phone:"",
        cpassword:"",
        address:"",
    });



    useEffect(() => {
        setUser({...user,hobbies:hobbies});
        return () => setUser({...user,hobbies:hobbies});
    }, [hobbies]);

    const getValue = (e) => {

        let value = e.target.value;
        if(hobbies.includes(value)){
            hobbies.pop(value);
        }
        else {
            hobbies.push(value);
        }

        if(hobbies.length < 2)
        {
            setError('At lest two checked');
            return false;
        }
        else {
            setError('');
            return true;
            // clearError('invalid-feedback');
        }
    }


    const fileUpload = (e) => {
        const filename = e.target.files[0].name;

        if(filename === ''){
            setImage( 'The Image Feild is required' );
            return false;
        }
        else if (!filename.match(/\.(jpg|jpeg|png)$/)) {
            setImage( 'Please select valid image.' );
            return false;
        }
        else {
            setImage('');
            const file = e.target.files[0];
            console.log(file);
            setUser({...user,image:file});
            return true;
        }

    };


    const {name,email,gender,phone,cpassword,address} = user;
    const changeInput = event => {
        setUser({...user,[event.target.name]:event.target.value});
    };


    const onsubmit = async (event) => {
        alert('in the submit');

        // const filename = e.target.files[0].name;
        //
        // if(filename === ''){
        //     setImage( 'The Image Feild is required' );
        //     return false;
        // }
        // else if (!filename.match(/\.(jpg|jpeg|png)$/)) {
        //     setImage( 'Please select valid image.' );
        //     return false;
        // }
        // else {
        //     setImage('');
        //     const file = e.target.files[0];
        //     console.log(file);
        //     setUser({...user,image:file});
        //     return true;
        // }



        const data = new FormData();
        data.append('name', user.name);
        data.append('email', user.email);
        data.append('image', user.image);
        data.append('gender', user.gender);
        data.append('hobbies', user.hobbies);
        data.append('phone', user.phone);
        data.append('cpassword', user.cpassword);
        data.append('address', user.address);


        event.preventDefault();
        const token = sessionStorage.getItem('token');
        console.log('token----',token);

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        console.log('config---',config);
        alert(config);

        const res =  await axios.post("/user",data,config);
        console.log('user--------',user);
        if(res){
            history.push("/");
        }
    };

    const {register,handleSubmit,errors,watch,clearError} = useForm({
        mode:"onTouched",
    });

    const password = useRef({});
    password.current = watch("password", "");

    return(
        <div className="container mt-5">
            <form name="form" className="form" onSubmit={handleSubmit( event => onsubmit(event))}>
                <fieldset>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text"
                               className={classNames("form-control",{"is-invalid":errors.name, })}
                               id="name"
                               name="name"
                               onChange={event => changeInput(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   minLength:{
                                       value:4,
                                       message:"Name should be more than 4 characters",
                                   },
                                   maxLength:{
                                       value:20,
                                       message:"Name should be less than 20 characters",
                                   },
                               })}
                               placeholder="Enter Name"/>
                        {errors.name && (
                            <div className="invalid-feedback">
                                {errors.name.message}
                            </div>
                        )}
                    </div>

                    <div className="form-group">
                        <label>Email address</label>
                        <input type="email"
                               className={classNames("form-control",{"is-invalid":errors.email, })}
                               id="email" name="email"
                               onChange={event => changeInput(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   pattern:{
                                       value:/^[a-zA-Z.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.['com' {|} 'in']+)*$/,
                                       message:"please enter a valid email"
                                   },
                               })}
                               placeholder="Enter email"/>
                        {errors.email && (
                            <div className="invalid-feedback">
                                {errors.email.message}
                            </div>
                        )}
                    </div>

                    <div className="form-group">
                        <label>Image</label>
                        <input type="file"
                               className={classNames("form-control",{"is-invalid":errors.image, })}
                               id="image"
                               name="image"
                               ref={register({
                                   required:"This Field is Required",
                               })}
                               onChange={fileUpload}/>
                        {errors.image && (
                            <div className="invalid-feedback">
                                {errors.image.message}
                            </div>
                        )}
                        {
                            image !== '' ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {image}
                                </div>
                                : null
                        }
                    </div>

                    <div className="form-group">
                        <label> Please select a Gender </label><br/>
                        <input type="radio"
                               value="male"
                               className={classNames({"is-invalid":errors.gender, })}
                               id="male"
                               name="gender"
                               ref={register({
                                   required:"This Field is Required",
                               })}
                               onChange={event => changeInput(event)} />Male
                        <input type="radio" value="female"
                               id="female" name="gender"
                               onChange={event => changeInput(event)} />Female
                        {errors.gender && (
                            <div className="invalid-feedback">
                                {errors.gender.message}
                            </div>
                        )}
                    </div>


                    <div className="form-group">
                        <label> Please select a Hobbies </label><br/>
                        <input
                            type="checkbox"
                            color="primary"
                            className={classNames("checkbox",{"is-invalid":errors.hobbies, })}
                            name="hobbies"
                            ref={register({
                                required:"This Field is Required",
                            })}
                            value="reading"
                            onChange={(e) => getValue(e)}/>Reading
                        <input
                            type="checkbox"
                            color="primary"
                            name="hobbies"
                            value="coding"
                            onChange={(e) => getValue(e)}/>Coding
                        <input
                            type="checkbox"
                            color="primary"
                            name="hobbies"
                            value="dancing"
                            onChange={(e) => getValue(e)}/>dancing
                        <input
                            type="checkbox"
                            color="primary"
                            name="hobbies"
                            value="cooking"
                            onChange={(e) => getValue(e)}/>Cooking
                        {console.log("Display Error-------",error)}
                        {errors.hobbies && (
                            <div className="invalid-feedback">
                                {errors.hobbies.message}
                            </div>
                        )}
                        {
                            error !== '' ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {error}
                                </div>
                                : null
                        }
                    </div>


                    <div className="form-group">
                        <label>Phone</label>
                        <input type="text"
                               className={classNames("form-control",{"is-invalid":errors.phone, })}
                               id="phone" name="phone"
                               onChange={event => changeInput(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   pattern:{
                                       value:/^\+(?:[0-9] ?){11}[0-9]$/,
                                       message:"please enter a valid Mobile Number"
                                   },
                               })}
                               placeholder="Enter phone"/>
                        {errors.phone && (
                            <div className="invalid-feedback">
                                {errors.phone.message}
                            </div>
                        )}
                    </div>

                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input type="password"
                               className={classNames("form-control",{"is-invalid":errors.password, })}
                               id="password" name="password"
                               onChange={event => changeInput(event)}
                               ref={register({
                                   required:"This Field is Required",
                                   minLength:{
                                       value:8,
                                       message:"Name should be more than 8 characters",
                                   },
                               })}
                               placeholder="Password"/>
                        {errors.password && (
                            <div className="invalid-feedback">
                                {errors.password.message}
                            </div>
                        )}
                    </div>

                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input type="password"
                               className={classNames("form-control",{"is-invalid":errors.cpassword, })}
                               id="cpassword" name="cpassword"
                               onChange={event => changeInput(event)}
                               ref={register({
                                   validate:value =>
                                       value === password.current || "The Password Does Not Match"
                               })}
                               placeholder="Confirm Password"/>
                        {errors.cpassword && (
                            <div className="invalid-feedback">
                                {errors.cpassword.message}
                            </div>
                        )}

                    </div>

                    <div className="form-group">
                        <label htmlFor="exampleTextarea">Address</label>
                        <textarea
                            className={classNames("form-control",{"is-invalid":errors.address, })}
                            id="address" name="address" rows="2"
                            onChange={event => changeInput(event)}
                            ref={register({
                                required:"This Field is Required",
                                minLength:{
                                    value:20,
                                    message:"Name should be more than 20 characters",
                                },
                                maxLength:{
                                    value:200,
                                    message:"Name should be less than 200 characters",
                                },
                            })}
                            placeholder="Address"></textarea>
                        {errors.address && (
                            <div className="invalid-feedback">
                                {errors.address.message}
                            </div>
                        )}
                    </div>
                    <Link className="btn btn-warning" to="/">
                        Back To Home
                    </Link>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </fieldset>
            </form>
        </div>
    );
};

export default AddUser;
