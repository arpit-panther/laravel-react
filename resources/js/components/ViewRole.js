import React,{useRef,useState,useEffect} from 'react';
import {Link, useHistory,useParams} from 'react-router-dom';
import { Checkbox } from '@material-ui/core';
import {useForm} from "react-hook-form";
import classNames from 'classnames';
import axios from "axios";
import Table from 'react-bootstrap/Table';



const ViewRole = (match) => {
    let params = match.params;
    let history = useHistory();
    let {id} = useParams();

    const [rolepermission,setRolePermission] = useState();

    const [permissions,setPemissions] = useState([]);

    const [perm,setPerm] = useState([]);

    const [roles,setRoles] = useState({
        name:"",
        permissions:"",
    });



    useEffect( () => {
        loadRoles();
        getPermission();
        setRoles({...roles,permissions:perm});
        return () => setRoles({...roles,permissions:perm})
    },[perm]);



    const loadRoles = async () => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        const result = await axios.get(`api/role/${id}/edit`,config);
        const data = result.data.data;
        setRoles(data.role);
        const rolepermission = Object.keys(data.rolepermission);
        setRolePermission(rolepermission);
    };



    const getPermission = async (event) => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        let url = 'api/role/create?' + new URLSearchParams().toString();
        const res = await axios.get(url, config);
        setPemissions(res.data.data);
    };




    let permi = [];
    const getValue = (e) => {
        let value = e.target.value;
        permi = rolepermission;
        if(permi.includes(value)){
            let store =  permi.filter(per => per !== value);
            permi = store;
        }
        else {
            permi.push(value);
        }
        setRoles({...roles,permissions: permi});
    };



    const changeInput = event => {
        setRoles({...roles,[event.target.name]:event.target.value});
    };



    const onsubmit = async (event) => {
        event.preventDefault();
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        console.log('data at submit',roles);
        const result = await axios.put(`api/role/${id}`,roles,config);
        if(result.status === 200){
            history.push("/Roles");
        }
    };




    const {handleSubmit} = useForm({
        mode:"onTouched",
    });


    return(
        <div className="container mt-5">
            {/*<form name="form" className="form" onSubmit={handleSubmit( event => onsubmit(event))}>*/}
            <form onSubmit={event => onsubmit(event)}>
                <fieldset>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text"
                               className={classNames("form-control")}
                               id="name"
                               name="name"
                               value={roles.name}
                               onChange={event => changeInput(event)}
                               placeholder="Enter Name"/>

                    </div>

                    <Table striped bordered hover variant="dark">
                        <thead>
                        <tr>
                            <th>Module</th>
                            <th>Permission</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            permissions && permissions.map( (permission) => (
                                <tr>
                                    <td>{permission.label}</td>
                                    {
                                        permission.data && permission.data.map((permissionDetail) => (
                                            <td>
                                                <input type="checkbox" id="permission" name="permission"
                                                       value={permissionDetail.permission} onChange={(e) => getValue(e)}
                                                />
                                                <span className="label-text">{permissionDetail.label}</span>
                                            </td>
                                        ))
                                    }
                                </tr>
                            ))
                        }
                        </tbody>
                    </Table>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </fieldset>
            </form>
        </div>
    );
};

export default ViewRole;
