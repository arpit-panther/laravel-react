import React,{useState,useEffect} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import {Button} from "react-bootstrap";
// import SweetAlert from 'react-bootstrap-sweetalert';
import SweetAlert from 'sweetalert2-react';


const User = () => {

    const [users,setUser] = useState([]);

    const[showw,setShow] = useState(false);
    const[deleteuser,setdeleteUser] = useState({});



    function deleteUser (id) {
        const res =  axios.delete(`api/user/${id}`);
        loadUsers();
    };

    useEffect(() => {
        loadUsers();
    },[]);

    const loadUsers = async () => {
        const res = await axios.get('api/user');
        if(res.data.status === 200){
            setUser(res.data.data);
        }
    };

    const abc = () => {

    }

    return(
        <div className="container">
            <div className="py-4">
                <h1>Home page</h1>
                <table className="table border shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Roles</th>
                        <th scope="col">Address</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        users && users.map((user,index) => (
                            <tr key={index}>
                                <th scope="row">{index+1}</th>
                                <th>{user.name}</th>
                                <th>{user.email}</th>
                                <th>{user.phone}</th>
                                <th>{user.roles}</th>
                                <th>{user.address}</th>
                                <td>
                                    <Link to={`/users/${user.id}`}>
                                        <Button className="btn btn-primary mr-2"> View </Button>
                                    </Link>
                                    <Link to={`/users/edit/${user.id}`}>
                                        <Button className="btn btn-warning mr-2"> Edit </Button>
                                    </Link>
                                    {/*<Button className="btn btn-danger" onClick={() => deleteUser(user.id)}> Delete </Button>*/}
                                    <Button className="btn btn-danger" onClick={ () => {setShow(true); setdeleteUser(user)} }> Delete </Button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                    <SweetAlert
                        show={showw}
                        title="Are You "
                        text="SweetAlert in React"
                        showCancelButton
                        onConfirm={() => {
                            console.log('confirm');
                            // this.setState({ show: false });
                        }}
                        onCancel={() => {
                            setShow(false);
                            console.log('cancel');

                            // this.setState({ show: false });
                        }}
                        // onEscapeKey={() => this.setState({ show: false })}
                        // onOutsideClick={() => this.setState({ show: false })}
                    />
                </table>
            </div>
        </div>
    );

};

export default User;
