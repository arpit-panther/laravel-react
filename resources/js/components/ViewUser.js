import React,{useEffect,useState} from 'react';
import axios from 'axios';
axios.defaults.baseURL = 'http://laravel-react.test';
import {Link,useParams} from "react-router-dom";
// import * as imagesss from "../../../public/images/wolf.png";


const ViewUser = (match) => {

    let params = match.params;

    // let url = process.env.MIX_APP_URL+'/';
    // console.log('url---',url);

    const [user,setUser] = useState({
        name:"",
        email:"",
        phone:"",
        password:"",
        address:"",
    });


    const {id} = useParams();


    useEffect(() => {
        loadUser();
    },[]);


    const loadUser = async () => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        const res = await axios.get(`api/user/${id}`,config);
        setUser(res.data);
    };

    return(
        <div className="container">
            <div className="card mb-3 mt-5 w-50">
                <h3 className="card-header">{user.name}</h3>
                <div className="card-body">
                    <h5 className="card-title">{user.email}</h5>
                    <h6 className="card-title">{user.phone}</h6>
                    <h6 className="card-title">Role:  {user.roles}</h6>
                </div>
                <img src={user.image} />
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">Gender : {user.gender}</li>
                    <li className="list-group-item">Hobbies : {user.hobbies}</li>
                    <li className="list-group-item">Address : {user.address}</li>
                </ul>
                <Link className="btn btn-primary" to="/Users">
                    Back To Home
                </Link>
            </div>
        </div>
    );
};

export default ViewUser;