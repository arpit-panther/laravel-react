import React,{useState,useEffect} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import {Button} from "react-bootstrap";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';
import $ from 'jquery';

window.React = React;

const User = () => {

    const MySwal = withReactContent(Swal);

    const [users,setUser] = useState([]);

    const [page,setPage] = useState(undefined);

    const perpage = 5 ;

    const [currentPage, setCurrentPage] = useState(0);


    function handlePageClick({ selected: selectedPage }) {
        setCurrentPage(selectedPage);
    }

    function deleteUser (id) {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                const res =  axios.delete(`api/user/${id}`);
                loadUsers();
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        });
    }


    useEffect(() => {
        loadUsers();
    },[]);


    const offset = currentPage * perpage;
    console.log('offset------>',offset);

    const loadUsers = async () => {
        const res = await axios.get('api/user',{
            params:{
                perpage: perpage,
                // page : page,
                offset:5,
            }
        });
        const result = res.data.data;
        console.log('result-----',result);
        const user =  result.slice(offset, offset + perpage);
        console.log('user-----',user);
        setUser(user);
        setPage(Math.ceil(result.length / perpage));
    };

    return(
        <div className="container">
            <div className="py-4">
                <h1>Home page</h1>
                <table className="table border shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Roles</th>
                        <th scope="col">Address</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        users && users.map((user,index) => (
                            <tr key={index}>
                                <th scope="row">{index+1}</th>
                                <th>{user.name}</th>
                                <th>{user.email}</th>
                                <th>{user.phone}</th>
                                <th>{user.roles}</th>
                                <th>{user.address}</th>
                                <td>
                                    <Link to={`/users/${user.id}`}>
                                        <Button className="btn btn-primary mr-2"> View </Button>
                                    </Link>
                                    <Link to={`/users/edit/${user.id}`}>
                                        <Button className="btn btn-warning mr-2"> Edit </Button>
                                    </Link>
                                    <Button className="btn btn-danger" onClick={() => deleteUser(user.id)}> Delete </Button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                <ReactPaginate
                    previousLabel={'previous'}
                    nextLabel={'next'}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    pageCount={page}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageClick}
                    containerClassName={'pagination'}
                    subContainerClassName={'pages pagination'}
                    activeClassName={'active'}
                />
            </div>
        </div>
    );
};

export default User;




//2




import React,{useState,useEffect} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import {Button} from "react-bootstrap";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content';
import Pagination from 'react-js-pagination';

window.React = React;

const User = () => {

    const MySwal = withReactContent(Swal);

    const [users,setUser] = useState([]);

    const perpage = 2 ;
    const [currentPage, setCurrentPage] = useState(1);
    const [url, setUrl] = useState(window.origin+`/api/user?page=${currentPage}`);

    // console.log('currentpage---',currentPage);


    function deleteUser (id) {

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                const res =  axios.delete(`api/user/${id}`);

                loadUsers();
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        });
    }


    useEffect(() => {
        loadUsers();
    },[]);

    const loadUsers = async () => {
        console.log('msg---',url);
        const {data} = await axios.get(url,{
            params:{
                perpage: perpage,
            }
        });

        console.log('----1-----',data.data.next_page_url);
        setUrl(data.data.next_page_url);
        setCurrentPage(data.data.current_page);
        console.log('2-------',data.data);
        setUser(data.data.data);
    };


    return(
        <div className="container">
            <div className="py-4">
                <h1>Home page</h1>
                <table className="table border shadow">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Roles</th>
                        <th scope="col">Address</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        users && users.map((user,index) => (
                            <tr key={index}>
                                <th scope="row">{index+1}</th>
                                <th>{user.name}</th>
                                <th>{user.email}</th>
                                <th>{user.phone}</th>
                                <th>{user.roles}</th>
                                <th>{user.address}</th>
                                <td>
                                    <Link to={`/users/${user.id}`}>
                                        <Button className="btn btn-primary mr-2"> View </Button>
                                    </Link>
                                    <Link to={`/users/edit/${user.id}`}>
                                        <Button className="btn btn-warning mr-2"> Edit </Button>
                                    </Link>
                                    <Button className="btn btn-danger" onClick={() => deleteUser(user.id)}> Delete </Button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                <Pagination
                    activePage={currentPage}
                    totalItemsCount={12}
                    itemsCountPerPage={perpage}
                    onChange={() => {loadUsers()}}
                    itemClass="page-item"
                    linkClass="page-link"
                    firstPageText="First"
                    lastPageText="Last"
                />
            </div>
        </div>
    );
};

export default User;
