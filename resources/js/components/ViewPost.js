import React,{useEffect,useState} from 'react';
import axios from 'axios';
axios.defaults.baseURL = 'http://laravel-react.test';
import {Link,useParams} from "react-router-dom";
// import * as imagesss from "../../../public/images/wolf.png";


const ViewPost = (match) => {

    let params = match.params;

    const [post,setPost] = useState({
        title:"",
        description:"",
    });


    const {id} = useParams();


    useEffect(() => {
        loadPost();
    },[]);


    const loadPost = async () => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        const res = await axios.get(`api/post/${id}`,config);
        setPost(res.data.data);
    };

    return(
        <div className="container">
            <div className="card mb-3 mt-5 w-50">
                <h3 className="card-header">{post.title}</h3>
                <div className="card-body">
                </div>
                <img src={post.image} />
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">Description : {post.description}</li>
                </ul>
                <Link className="btn btn-primary" to="/Posts">
                    Back To Home
                </Link>
            </div>
        </div>
    );
};

export default ViewPost;