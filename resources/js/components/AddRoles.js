import React,{useRef,useState,useEffect} from 'react';
import {Link, useHistory} from 'react-router-dom';
import { Checkbox } from '@material-ui/core';
import {useForm} from "react-hook-form";
import classNames from 'classnames';
import axios from "axios";
import Table from 'react-bootstrap/Table';
import Swal from "sweetalert2";



const AddUser = () => {
    let history = useHistory();
    const [hobbies, setHobbies] = useState([]);

    const [permissions,setPemissions] = useState([]);

    const [perm,setPerm] = useState([]);

    const [roles,setRoles] = useState({
        name:"",
        permissions:"",
    });

    useEffect( () => {
        const token = sessionStorage.getItem('token');
        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        let data = "test";
        const result = axios.post("api/role",data,config)
            .then( function () {

            })
            .catch(function (error) {
                if (error.response.status === 401) {
                    let timerInterval
                    Swal.fire({
                        title: 'Oops!! You Have No Access To THis Page',
                        timer: 1000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                    });
                    history.push('/');
                }
            });

        getPermission();
        setRoles({...roles,permissions:perm});
        return () => setRoles({...roles,permissions:perm})
    },[perm]);


    const getValue = (e) => {
        let value = e.target.value;
        console.log('value--->>',value);
        if(perm.includes(value)){
            perm.pop(value);
        }
        else {
            perm.push(value);
        }
    };

    const getPermission = async (event) => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        let url = 'api/role/create?' + new URLSearchParams().toString();
        const res = await axios.get(url, config);
        setPemissions(res.data.data);
    };

    const changeInput = event => {
        setRoles({...roles,[event.target.name]:event.target.value});
        console.log('roles->',roles);
    };

    const onsubmit = async (event) => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        const data = new FormData();
        data.append('name', roles.name);
        data.append('permissions', roles.permissions);
        const result = await axios.post("api/role",data,config);
        if(result.status === 200){
            history.push("/Roles");
        }
        console.log('roles-result->',result);
    };

    const {handleSubmit} = useForm({
        mode:"onTouched",
    });

    return(
        <div className="container mt-5">
            <form name="form" className="form" onSubmit={handleSubmit( event => onsubmit(event))}>
                <fieldset>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text"
                               className={classNames("form-control")}
                               id="name"
                               name="name"
                               onChange={event => changeInput(event)}
                               placeholder="Enter Name"/>
                    </div>

                    <Table striped bordered hover variant="dark">
                        <thead>
                        <tr>
                            <th>Module</th>
                            <th>Permission</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            permissions && permissions.map( (permission) => (
                                <tr>
                                    <td>{permission.label}</td>
                                    {
                                        permission.data && permission.data.map((permissionDetail) => (
                                        <td>
                                            <input type="checkbox" id="permission" name="permission" value={permissionDetail.permission} onChange={(e) => getValue(e)} />
                                            <span className="label-text">{permissionDetail.label}</span>
                                        </td>
                                        ))
                                    }
                                </tr>
                            ))
                        }
                        </tbody>
                    </Table>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </fieldset>
            </form>
        </div>
    );
};

export default AddUser;
