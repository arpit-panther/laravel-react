import React,{useState,useEffect} from 'react';
import {useHistory,useParams} from 'react-router-dom';
import axios from "axios";
axios.defaults.baseURL = 'http://laravel-react.test';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content';
import {Checkbox} from "@material-ui/core";



const EditUser = (match) => {
    const {id} = useParams();
    const MySwal = withReactContent(Swal);
    let params = match.params;


    const [errors,setError] = useState(null);
    const [roles,setRoles] = useState([]);
    const [hobbies, setHobbies] = useState([]);
    let history = useHistory();
    const [tosendrol,setToSendRole] = useState({
        id:"",
    });
    const [user,setUser] = useState({
        name:"",
        email:"",
        image:"",
        gender:"",
        hobbies:"",
        phone:"",
        password:"",
        address:"",
    });



    let tempHobbies = [];

    const getValue = (e) => {
        let value = e.target.value;
        tempHobbies = user.hobbies;

        if(tempHobbies.includes(value)){
            let store =  tempHobbies.filter(hobby => hobby !== value);
            tempHobbies = store;
        }
        else {
            tempHobbies.push(value);
            }
        setUser({...user,hobbies: tempHobbies});
    };



    let temproles = [];

    const saveRole = (e) => {
        let value = e.target.value;
        temproles = tosendrol.id;

        if(temproles.includes(value)){
            let store =  temproles.filter(roless => roless !== value);
            temproles = store;
        }
        else {
            temproles.push(value);
        }
        setToSendRole({...tosendrol,id:temproles});
    };




    const changeInput = event => {
        setUser({...user,[event.target.name]:event.target.value});
    };

    useEffect(() => {
        const token = sessionStorage.getItem('token');
        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        let data = "test";
        const res =  axios.put(`api/user/${id}`, user, config)
            .then( function () {

            })
            .catch(function (error) {
                if (error.response.status === 401) {
                    let timerInterval
                    Swal.fire({
                        title: 'Oops!! You Have No Access To THis Page',
                        timer: 1000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent()
                                if (content) {
                                    const b = content.querySelector('b')
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft()
                                    }
                                }
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                    });
                    history.push('/');
                }
            });
        getRoles();
        getUserRoles();
        loadUser();
    },[]);


    const getRoles = async () => {
        const token = sessionStorage.getItem('token');
        const config = {
            headers: {Authorization: `Bearer ${token}`}
        };
        let url = 'api/rolesname?' + new URLSearchParams().toString();
        const res = await axios.get(url, config);
        const result = res.data.data;
        setRoles(result);
    };


    const getUserRoles = async() => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        const res = await axios.get(`api/userrole/${id}`,config);

        const roleidd = res.data;

        setToSendRole({...tosendrol,id: roleidd});
    };


    const loadUser = async() => {
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        const res = await axios.get(`api/user/${id}/edit`,config);
        setUser(res.data);
    };


    const fileUpload = (e) => {
        const file = e.target.files[0].name;
        setUser({...user,image:file});
    };




    const onsubmit  = async (event) => {
        event.preventDefault();
        const token = sessionStorage.getItem('token');

        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };

        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
        }).then(async (result) => {
            const res = await axios.put(`api/user/${id}`, user, config);
            const roooole = tosendrol;
            const resroles = await axios.put(`api/updaterole/${id}`, roooole, config);
            if (result.isConfirmed) {
                Swal.fire('Saved!', '', 'success')
                history.push("/");

            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
            .catch(function (error) {if (error.response) {
                const new_error = error.response.data.errors;
                setError(new_error)
            }});
    };





    return(
        <div className="container mt-5">
            <form onSubmit={event => onsubmit(event)}>
                <fieldset>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Email address</label>
                        <input type="email"
                               className="form-control"
                               id="email" name="email"  value={ user.email}
                               onChange={event => changeInput(event)} placeholder="Enter email"/>
                        {
                            errors && errors.email ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.email}
                                </div>
                                : null
                        }
                    </div>


                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input type="password"
                               className="form-control"
                               id="password" name="password"
                               onChange={event => changeInput(event)} placeholder="Enter Your Password If You Want to change"/>
                    </div>


                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">First Name</label>
                        <input type="text"
                               className="form-control"
                               id="first_name" name="first_name"
                               value={user.first_name}
                               onChange={event => changeInput(event)} placeholder="Enter First Name"/>
                        {
                            errors && errors.first_name ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.first_name}
                                </div>
                                : null
                        }
                    </div>


                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Last Name</label>
                        <input type="text"
                               className="form-control"
                               id="last_name" name="last_name"
                               value={user.last_name}
                               onChange={event => changeInput(event)} placeholder="Enter Last Name"/>
                        {
                            errors && errors.last_name ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.last_name}
                                </div>
                                : null
                        }
                    </div>


                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Image</label>
                        <input type="file"
                               className="form-control"
                               id="image" name="image"  onChange={fileUpload} placeholder="Enter image"/>
                        {
                            errors && errors.file ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.file}
                                </div>
                                : null
                        }
                    </div>


                    <div className="form-group">
                        <label> Please select a Gender </label><br/>
                        <input type="radio" value="male"
                               id="male" name="gender"
                               checked={user.gender === "male"}

                               onChange={event => changeInput(event)} />Male
                        <input type="radio" value="female"
                               id="female" name="gender"
                               checked={user.gender === "female"}
                               onChange={event => changeInput(event)} />Female
                        {
                            errors && errors.gender ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.gender}
                                </div>
                                : null
                        }
                    </div>


                    {/*<div className="form-group">*/}
                        {/*<select multiple="" className="custom-select" name="roles"  onChange={event => changeInput(event)}>*/}
                            {/*/!*<option value="">Select A Role</option>*!/*/}
                            {/*{*/}
                                {/*roles && roles.map((role,index) => (*/}
                                    {/*<option value={role.id} key={role.id}>{role.name}</option>*/}
                                {/*))*/}
                            {/*}*/}
                        {/*</select>*/}
                    {/*</div>*/}



                    <div className="form-group">
                        <label> Select Your Roles </label><br/>
                        {
                            roles && roles.map((role,index) => (
                                <div key={index}>
                                    <input type="checkbox" id="roles" name="roles"
                                           value={role.id}
                                           checked={tosendrol.id.includes(role.id.toString())}
                                    onChange={(e) => saveRole(e)}
                                    />
                                    {role.name}
                                </div>
                            ))
                        }
                    </div>


                    <div className="form-group">
                        <label> Please select a Hobbies </label><br/>
                        <Checkbox
                            color="primary"
                            value="reading"
                            checked={user.hobbies.includes("reading")}
                            onChange={(e) => getValue(e)}
                        />Reading
                        <Checkbox
                            color="primary"
                            value="coding"
                            checked={user.hobbies.includes("coding")}
                            onChange={(e) => getValue(e)}
                        />Coding
                        <Checkbox
                            color="primary"
                            value="dancing"
                            checked={user.hobbies.includes("dancing")}
                            onChange={(e) => getValue(e)}
                        />dancing
                        <Checkbox
                            color="primary"
                            value="cooking"
                            checked={user.hobbies.includes("cooking")}
                            onChange={(e) => getValue(e)}
                        />Cooking
                        {
                            errors && errors.hobbies ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.hobbies}
                                </div>
                                : null
                        }
                    </div>


                    <button type="submit" className="btn btn-primary">Submit</button>
                </fieldset>
            </form>
        </div>
    );
};

export default EditUser;
