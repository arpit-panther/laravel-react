import React,{useRef,useState,useEffect} from 'react';
import {Link, useHistory} from 'react-router-dom';
import { Checkbox } from '@material-ui/core';
import {useForm} from "react-hook-form";
import classNames from 'classnames';
import axios from "axios";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content';


const Register = () => {
    let history = useHistory();
    const [hobbies, setHobbies] = useState([]);
    const [errors,setError] = useState(null);
    const MySwal = withReactContent(Swal);

    const [user,setUser] = useState({
        email:"",
        password:"",
        password_confirmation:"",
        first_name:"",
        last_name:"",
        image:"",
        gender:"",
        hobbies:"",
    });

    useEffect(() => {
        setUser({...user,hobbies:hobbies});
        return () => setUser({...user,hobbies:hobbies});
    }, [hobbies]);

    const getValue = (e) => {

        let value = e.target.value;
        if(hobbies.includes(value)){
            hobbies.pop(value);
        }
        else {
            hobbies.push(value);
        }

    };


    const fileUpload = (e) => {
        const file = e.target.files[0];
        setUser({...user,image:file});
    };


    const {name,email,gender,phone,cpassword,address} = user;
    const changeInput = event => {
        setUser({...user,[event.target.name]:event.target.value});
        console.log(user);
    };


    const onsubmit = async (event) => {

        const data = new FormData();
        data.append('email', user.email);
        data.append('password', user.password);
        data.append('password_confirmation', user.password_confirmation);
        data.append('first_name', user.first_name);
        data.append('last_name', user.last_name);
        data.append('image', user.image);
        data.append('gender', user.gender);
        data.append('hobbies', user.hobbies);

        console.log('data----',data);

        const res = await axios.post("api/register",data);
        console.log('res----------->>',res);
        if (res.status === 200){
            Swal.fire(
                'Good job!',
                'You Are Now A Member Of Our Family..!!',
                'success'
            );
            history.push("/");
        }
        else{

        }
    };

    const {register,handleSubmit,watch,clearError} = useForm({
        mode:"onTouched",
    });

    const password = useRef({});
    password.current = watch("password", "");

    return(
        <div className="container mt-5">
            <form name="form" className="form" onSubmit={handleSubmit( event => onsubmit(event))}>
                <fieldset>
                    <div className="form-group">
                        <label>Email address</label>
                        <input type="email"
                               className={classNames("form-control")}
                               id="email" name="email"
                               onChange={event => changeInput(event)}
                               placeholder="Enter email"/>

                        {
                            errors && errors.email ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.email}
                                </div>
                                : null
                        }
                    </div>


                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input type="password"
                               className={classNames("form-control")}
                               id="password" name="password"
                               onChange={event => changeInput(event)}
                               placeholder="Password"/>

                        {
                            errors && errors.password ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.password}
                                </div>
                                : null
                        }
                    </div>



                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password Confirmation</label>
                        <input type="password"
                               className={classNames("form-control")}
                               id="password_confirmation" name="password_confirmation"
                               onChange={event => changeInput(event)}
                               placeholder="Confirm Password"/>

                        {/*{*/}
                        {/*errors && errors.cpassword ?*/}
                        {/*<div className="invalid-feedback" style={{display:'block'}}>*/}
                        {/*{errors.cpassword}*/}
                        {/*</div>*/}
                        {/*: null*/}
                        {/*}*/}

                    </div>



                    <div className="form-group">
                        <label>First Name</label>
                        <input type="text"
                               className={classNames("form-control")}
                               id="first_name"
                               name="first_name"
                               onChange={event => changeInput(event)}
                               placeholder="Enter First Name"/>

                        {
                            errors && errors.first_name ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.first_name}
                                </div>
                                : null
                        }

                    </div>


                    <div className="form-group">
                        <label>Last Name</label>
                        <input type="text"
                               className={classNames("form-control")}
                               id="last_name"
                               name="last_name"
                               onChange={event => changeInput(event)}
                               placeholder="Enter Last Name"/>

                        {
                            errors && errors.last_name ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.last_name}
                                </div>
                                : null
                        }
                    </div>


                    <div className="form-group">
                        <label>Image</label>
                        <input type="file"
                               className={classNames("form-control")}
                               id="image"
                               name="image"
                               onChange={fileUpload}/>

                        {
                            errors && errors.image ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.image}
                                </div>
                                : null
                        }
                    </div>


                    <div className="form-group">
                        <label> Please select a Gender </label><br/>
                        <input type="radio"
                               value="male"
                               id="male"
                               name="gender"
                               onChange={event => changeInput(event)} />Male
                        <input type="radio" value="female"
                               id="female" name="gender"
                               onChange={event => changeInput(event)} />Female
                        {
                            errors && errors.gender ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.gender}
                                </div>
                                : null
                        }
                    </div>


                    <div className="form-group">
                        <label> Please select a Hobbies </label><br/>
                        <input
                            type="checkbox"
                            color="primary"
                            className={classNames("checkbox")}
                            name="hobbies"
                            value="reading"
                            onChange={(e) => getValue(e)}/>Reading
                        <input
                            type="checkbox"
                            color="primary"
                            name="hobbies"
                            value="coding"
                            onChange={(e) => getValue(e)}/>Coding
                        <input
                            type="checkbox"
                            color="primary"
                            name="hobbies"
                            value="dancing"
                            onChange={(e) => getValue(e)}/>dancing
                        <input
                            type="checkbox"
                            color="primary"
                            name="hobbies"
                            value="cooking"
                            onChange={(e) => getValue(e)}/>Cooking

                        {
                            errors && errors.hobbies ?
                                <div className="invalid-feedback" style={{display:'block'}}>
                                    {errors.hobbies}
                                </div>
                                : null
                        }

                    </div>


                    <Link className="btn btn-warning" to="/">
                        Back To Home
                    </Link>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </fieldset>
            </form>
        </div>
    );
};

export default Register;
