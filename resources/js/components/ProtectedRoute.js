import React from 'react';
    import {Switch, Route, HashRouter, Redirect} from "react-router-dom";
// import Nav from "./Nav";
// import User from "./User";
// import AddUser from "./AddUser";
// import ViewUser from "./ViewUser";
// import NotFound from "./NotFound";
// import EditUser from "./EditUser";
// import Logout from "./Logout";
// import Login from "./Login";
// import Posts from "./Posts";
// import Roles from "./Roles";
// import AddRoles from "./AddRoles";
// import EditRole from "./EditRole";
// import ViewRole from "./ViewRole";
// import AddPosts from "./AddPosts";
// import EditPost from "./EditPost";
// import ViewPost from "./ViewPost";



const ProtectedRoute = ( {component:Cmp,...rest}) => (

    <Route
        {...rest}
        render = {(props)=>
            sessionStorage.getItem('token') ?(
            <Cmp {...props} />
            ) :
                <Redirect
                to="/login"
                />
        }
    />

);


export default ProtectedRoute;