import React,{useRef,useState,useEffect} from 'react';
import {Link, useHistory} from 'react-router-dom';
import { Checkbox } from '@material-ui/core';
import {useForm} from "react-hook-form";
import classNames from 'classnames';
import axios from "axios";



const CheckboxPage = () => {
    let history = useHistory();
    const [hobbies, setHobbies] = useState([]);

    const [user,setUser] = useState({
        gender:""
    });


    const [error,setError] = useState("");


    useEffect(() => {
        setUser({...user,hobbies:hobbies});
        return () => setUser({...user,hobbies:hobbies});
    }, [hobbies])

    const changeInput = event => {
        setUser({...user,[event.target.name]:event.target.value});
    };

    const getValue = (e) => {

        let value = e.target.value;
        if(hobbies.includes(value)){
            hobbies.pop(value);
        }
        else {
            hobbies.push(value);
        }

        console.log('hobbies length------------',hobbies.length);
        if(hobbies.length < 2)
        {
            setError('At lest two checked');
            return false;
        }
        else {
            setError('');
            clearError('invalid-feedback');
        }
    }


    const {register,handleSubmit,errors,clearError} = useForm({
        mode:"onTouched",
    });

    return(
        <div className="container mt-5">
            <form name="form" className="form" onSubmit={handleSubmit( event => onsubmit(event))}>
                <fieldset>


                    <div className="form-group">
                        <label> Please select a Gender </label><br/>
                        <input type="radio"
                               value="male"
                               className={classNames({"is-invalid":errors.gender, })}
                               id="male"
                               name="gender"
                               ref={register({
                                   required:"This Field is Required",
                               })}
                               onChange={event => changeInput(event)} />Male
                        <input type="radio" value="female"
                               id="female" name="gender"
                               onChange={event => changeInput(event)} />Female
                        {errors.gender && (
                            <div className="invalid-feedback">
                                {errors.gender.message}
                            </div>
                        )}
                    </div>

                    <div className="form-group">
                        <label> Please select a Hobbies </label><br/>
                        <input
                            type="checkbox"
                            color="primary"
                            className={classNames("checkbox",{"is-invalid":errors.hobbies, })}
                            name="hobbies"
                            ref={register({
                                required:"This Field is Required",
                            })}
                            value="reading"
                            onChange={(e) => getValue(e)}/>Reading
                        <input
                            type="checkbox"
                            color="primary"
                            name="hobbies"
                            value="coding"
                            onChange={(e) => getValue(e)}/>Coding
                        <input
                            type="checkbox"
                            color="primary"
                            name="hobbies"
                            value="dancing"
                            onChange={(e) => getValue(e)}/>dancing
                        <input
                            type="checkbox"
                            color="primary"
                            name="hobbies"
                            value="cooking"
                            onChange={(e) => getValue(e)}/>Cooking
                        {console.log("Display Error-------",error)}

                        {
                            error !== '' ?
                            <div className="invalid-feedback" style={{display:'block'}}>
                                {error}
                            </div>
                            : null
                        }
                    </div>

                    <Link className="btn btn-warning" to="/">
                        Back To Home
                    </Link>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </fieldset>
            </form>
        </div>
    );
};

export default CheckboxPage;
