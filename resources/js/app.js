import CheckBox from "./components/CheckBoxPage";

require('./bootstrap');


import React,{useState} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router,Switch,Route,Redirect} from "react-router-dom";
import Nav from "./components/Nav";
import User from "./components/User";
import AddUser from "./components/AddUser";
import ViewUser from "./components/ViewUser";
import NotFound from "./components/NotFound";
import EditUser from "./components/EditUser";
import CheckBoxPage from "./components/CheckBoxPage";
import ForTest from "./components/ForTest";
import Logout from "./components/Logout";
import Login from "./components/Login";
import SweetAlert from 'sweetalert2-react';
import { HashRouter } from 'react-router-dom'
import ProtectedRoute from "./components/ProtectedRoute";
import Register from "./components/Register";
import DashBoard from "./components/DashBoard";
import Posts from "./components/Posts";
import AddPosts from "./components/AddPosts";
import EditPost from "./components/EditPost";
import ViewPost from "./components/ViewPost";
import EditRole from "./components/EditRole";
import ViewRole from "./components/ViewRole";
import Roles from "./components/Roles";
import AddRoles from "./components/AddRoles";



function App() {
    return (
            <>
                <Router>
                    <HashRouter>

                    <ProtectedRoute component={Nav}/>

                    {
                        sessionStorage.getItem('token') !== null ?
                        <Route exact path="/login" component={User} />
                        :
                        <Redirect exact from="/login" to="login" />
                    }

                    <Route path="/login" component={Login} />
                    <Route exact path="/logout" component={Logout} />


                    {/*-----------------USERS ROUTES----------------*/}

                    <ProtectedRoute exact path="/addUsers" component={AddUser} />
                    <ProtectedRoute exact path="/users/:id" component={ViewUser} />
                    <ProtectedRoute exact path="/users/edit/:id" component={EditUser} />
                    <ProtectedRoute exact path="/" component={User} />

                    {/*-----------------POST ROUTES----------------*/}
                    <ProtectedRoute exact path ="/posts" component={Posts} />
                    <ProtectedRoute exact path ="/addPosts" component={AddPosts} />
                    <ProtectedRoute exact path ="/post/edit/:id" component={EditPost} />
                    <ProtectedRoute exact path ="/posts/:id" component={ViewPost} />


                    {/*-----------------ROLES ROUTES----------------*/}
                    <ProtectedRoute exact path ="/role/edit/:id" component={EditRole} />
                    <ProtectedRoute exact path ="/roles/:id" component={ViewRole} />
                    <ProtectedRoute exact path ="/Roles" component={Roles} />
                    {/*<Route component={NotFound}/>*/}
                    </HashRouter>
                </Router>
            </>
    );
}



export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
