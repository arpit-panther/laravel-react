<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends SentinelUserModel
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = ['id','email','password','first_name','last_name','image','gender','roles','hobbies'];

}

