<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Sentinel;

class SentinelCheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        $user = Sentinel::getUser();
        $roles = Role::get()->pluck('slug')->toArray();
        foreach ($roles as $role){
            if(Sentinel::inRole($role)){
                return $next($request);
            }
        }
        return redirect('/Users');
    }
}
