<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Sentinel;
use Closure;
use Illuminate\Http\Request;

class SentinelUserHasAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $permission)
    {
//        dd($permission);
        $user = Auth::user();
        $id = $user->id;
        $user = Sentinel::findById($id);
        Sentinel::login($user);
        if(!Sentinel::check()){
            return $this->denied($request);
        }
        if(!Sentinel::hasAccess($permission)){
            return $this->denied($request);
        }
        return $next($request);
    }
    public function denied($request)
    {
        if ($request->ajax() || $request->wantsJson() || $request->isJson()) {
            $message = __('unauthorized');

            return response()->json([
                'success' => false,
                'type'    => 'error',
                'message' => $message,
            ], 401);
        }

        $message = 'You do not have permission to do that.';
        abort(401, $message);
    }
}
