<?php

namespace App\Http\Controllers;
use App\Http\Requests\StoreUserRequest;
use App\Models\Role;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Sentinel;

class UserController extends Controller
{

    public function __construct()
    {
//        $this->middleware('sentinel.auth');
        $this->middleware('sentinel.access:view_user_module',['only'=>['index']]);
        $this->middleware('sentinel.access:view_user',['only'=>['show']]);
        $this->middleware('sentinel.access:create_user',['only'=>['create','store']]);
        $this->middleware('sentinel.access:update_user',['only'=>['edit','update']]);
        $this->middleware('sentinel.access:delete_user',['only'=>['destroy']]);
//        $this->middleware('db_transaction:true',['only'=>['store','update','delete']]);
    }



    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $perpage = $request->perpage;
        $users = User::paginate($perpage);
//        dd('users------',$users);
        return response()->json(['status'=>200,'data'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $directory = 'images';
        Storage::makeDirectory($directory);

        $image=$request->image;
        $image_name = $image->getClientOriginalName();
        $destination_path = 'images';

        $store = "images/".$image_name;

        $path = Storage::putFileAs('images', $image,$image_name);

        $email = $request->email;
        $password = $request->password;
        $role =$request->roles;

        $rolearray = explode(",",$role);

        $credentials = [
            'email'    => $email,
            'password' => $password,
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'image' => $store,
            'gender'=>$request->gender,
            'hobbies'=>$request->hobbies,
        ];
        $user = Sentinel::register($credentials);

        if ($user){
            $register_user = User::where('email',$email)->first();
            $id = $register_user->id;
            $activeuser = Sentinel::findUserById($id);
            $activation = Activation::create($activeuser);
            Activation::complete($activeuser,$activation->code);

            foreach ($rolearray as $rol){
                $rol = Sentinel::findRoleById($rol);
                $rol->users()->attach($user);
            }
            DB::commit();
            return response()->json($activeuser,200);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $user->image = config("app.url").'/'.$user->image;
        return response()->json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
         $userhobby = explode(",",$user->hobbies);
         $user->hobbies = $userhobby;
        return response($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        $image=$request->image;
//        $image_name = $image->getClientOriginalName();
//        $destination_path = 'images';
//        $store = "images/".$image_name;
//        $path = Storage::putFileAs('images', $image,$image_name);

        $hobbies = implode(",",$request->hobbies);

        $user = User::find($id);
        $user->email =$request->email;
        $user->password = hash::make($request->password);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->gender = $request->gender;
        $user->hobbies=$hobbies;
        $user->save();
        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);
        $user->delete();
    }

    public function login(Request $request)
    {


        $email = $request->email;
        $password = $request->password;


        $credentials = [
            'email'    => $email,
            'password' => $password
        ];

        $user =  Sentinel::authenticate($credentials);

        $loginuser = Sentinel::login($user);

        if($loginuser){
            $token = $loginuser->createToken('login-token')->plainTextToken;
            $response = [
                'user' => $user,
                'token' => $token
            ];
            return response()->json($response,200);
        }

    }

    public function count()
    {
        $user = User::all();
        $count = count($user);
        if($count){
            return response($count);
        }
    }

    public function userrole($id)
    {
        $user = User::find($id);
        $roles = $user->roles;
        $sendrole = [];
        foreach ($roles as $role){
            $sendrole[] = strval($role->id);
        }

        return response()->json($sendrole,200);
    }

    public function updaterole(Request $request,$id)
    {
        $rolearray = $request->id;
        $user = Sentinel::findUserById($id);
        $roles = $user->roles;
        foreach ($roles as $role){
            $role = Sentinel::findRoleById($role->id);
            $role->users()->detach($user);
        }
        $numArray = array_map('intval',$rolearray);
        foreach ($numArray as $rol){
            $rol = Sentinel::findRoleById($rol);
            $rol->users()->attach($user);
        }
    }
}
