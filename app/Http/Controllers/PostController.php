<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{

    public function __construct()
    {
//        $this->middleware('sentinel.auth');
        $this->middleware('sentinel.access:view_post_module',['only'=>['index']]);
        $this->middleware('sentinel.access:view_post',['only'=>['show']]);
        $this->middleware('sentinel.access:create_post',['only'=>['create','store']]);
        $this->middleware('sentinel.access:update_post',['only'=>['edit','update']]);
        $this->middleware('sentinel.access:delete_post',['only'=>['destroy']]);
//        $this->middleware('db_transaction:true',['only'=>['store','update','delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        //
        $id = auth()->user()->id;
        $posts = Post::get()->where('user_id',$id);
        if ($posts){
            return response()->json(['status'=>200,'data'=>$posts]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        //

        $directory = 'images';
        Storage::makeDirectory($directory);

        $image=$request->image;
        $image_name = $image->getClientOriginalName();
        $destination_path = 'images';

        $store = "images/".$image_name;

        $id = Auth::id();
        $path = Storage::putFileAs('images', $image,$image_name);
        $posts = Post::create([
            'user_id' => $id,
            'title' => $request->title,
            'image' => $store,
            'description' => $request->description,
        ]);
        return $this->sendResponse($posts, 'Post Added successfully', Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        //
        $post = Post::find($id);
        $post->image = config("app.url").'/'.$post->image;
        return $this->sendResponse($post, 'Post Retrieved successfully', Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function edit($id)
    {
        //
        $posts = Post::find($id);
        return $this->sendResponse($posts, 'posts Retrived successfully', Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        //
        $post = Post::find($id);
        $post->title = $request->title;
//        dd($request->image);
//        $file = $request->image;
//        if ($file){
//            $image=$request->image;
//            $image_name = $image->getClientOriginalName();
//            $destination_path = 'images';
//            $store = "images/".$image_name;
//            $path = Storage::putFileAs('images', $image,$image_name);
//            $post->image = $store;
//        }
//        else{
//
//        }
        $post->description = $request->description;
//        dd($post);
            $post->save();
        return $this->sendResponse($post, 'posts Updated successfully', Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return $this->sendResponse($post, 'roles Deleted successfully', Response::HTTP_OK);
    }
}
