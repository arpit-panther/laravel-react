<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class RoleController extends Controller
{


    public function __construct()
    {
//        $this->middleware('sentinel.auth');
        $this->middleware('sentinel.access:view_role_module',['only'=>['index']]);
        $this->middleware('sentinel.access:view_role',['only'=>['show']]);
        $this->middleware('sentinel.access:create_role',['only'=>['create','store']]);
        $this->middleware('sentinel.access:update_role',['only'=>['edit','update']]);
        $this->middleware('sentinel.access:delete_role',['only'=>['destroy']]);
//        $this->middleware('db_transaction:true',['only'=>['store','update','delete']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $role = Role::all();
        if($role){
            return response()->json($role,200);
        }

    }




    public function roleName()
    {

        $datas = Role::all();

        $roles = [];

        foreach ($datas as $data){

            $roles[] =
                ['id' => $data->id,
                    'name' => $data->name
                ];
        }

        return $this->sendResponse($roles, 'roles retrieved successfully', Response::HTTP_OK);


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create()
    {
        $allPermissions = config('role-permission');
        return $this->sendResponse($allPermissions, 'roles retrieved successfully', Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $role = new Role();
        $role->name = $request->name;
        $role->slug = Str::slug($request->name);
        $permissions = $request->permissions;
        $lists = explode(",",$permissions);
        $store = [];
        foreach ($lists as $list)
        {
            $store[$list]=true;
        }
        $role->permissions = $store;
        $role->save();
        return $this->sendResponse($role, 'roles Submitted successfully', Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function edit($id)
    {
        //
        $role = Role::find($id);
        $rolepermission = $role->permissions;
        $allpermission = config('role-permission');
        $permission = compact(['rolepermission','allpermission','role']);
        return $this->sendResponse($permission, 'roles Submitted successfully', Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        //
        $roles = Role::find($id);
        $roles->name = $request->name;
        $roles->slug = Str::slug($request->name);
        $lists = $request->permissions;
        $store = [];
        foreach ($lists as $list)
        {
            $store[$list]=true;
        }
        $roles->permissions = $store;
        $roles->save();
        return $this->sendResponse($roles, 'roles Submitted successfully', Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        //
        $role = Role::find($id);
        $role->delete();
        return $this->sendResponse($role, 'roles Deleted successfully', Response::HTTP_OK);
    }
}
