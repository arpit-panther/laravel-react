<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Route;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->request->get('id');
        switch (Route::currentRouteName()){
            case 'user.store':
                {
                    $rules = [
                       'email'=>'required|min:5|max:25',
                        'password'=>'required|confirmed|min:8|max:15',
                        'first_name'=>'required|min:5|max:20',
                        'last_name'=>'required|min:5|max:20',
                        'image'=>'required|image|mimes:jpeg,png,jpg|max:2048',
                        'gender'=>'required',
                        'roles'=> 'required',
                        'hobbies'=>'required',
                    ];
                    return $rules;
                }
            case 'user.update':
                {
                    $rules = [
                        'name'=>'required|min:5|max:20',
                        'email'=>"required|min:5|max:25|unique:users,email,{$id},id",
//                        'image'=>'image|mimes:jpeg,png,jpg|max:2048',
                        'gender'=>'required',
                        'roles'=> 'required',
                        'hobbies'=>'required',
                        'phone'=>'required|min:10|max:13',
//                        'password'=>'required|min:8|max:15',
                        'address'=>'required|min:15|max:150',
                    ];
                    return $rules;
                }
            case 'user.register':
                {
                    $rules = [
                        'email'=>'required|min:5|max:25',
                        'password'=>'required|confirmed|min:8|max:15',
                        'first_name'=>'required|min:5|max:20',
                        'last_name'=>'required|min:5|max:20',
                        'image'=>'required|image|mimes:jpeg,png,jpg|max:2048',
                        'gender'=>'required',
                        'hobbies'=>'required',
                    ];
                    return $rules;
                }
            default :{
                break;
            }
        }

        return [
            //
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $response = new JsonResponse([
            'message' => 'The given data is invalid',
            'errors' => $validator->errors()
        ], 422);

        throw new ValidationException($validator, $response);
    }
}
